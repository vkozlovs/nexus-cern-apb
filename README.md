# nexus-cern-apb

An application template for deploying [Nexus3 OSS](https://www.sonatype.com/nexus-repository-oss) at CERN

## How to deploy an instance of Nexus

Nexus deployments use S3 for artifact storage and you need to provide valid S3 keys to provision an instance.
You will be the owner of the S3 storage and in control of the amount of quota assigned to it;
Nexus will only be using it to store its data.

1. Before creating an instance, [request S3 quota from CERN Cloud Services](http://clouddocs.web.cern.ch/clouddocs/object_store/index.html)
  for the amount of data intended to be stored in the instance. You may use an existing
  Openstack project if it is relevant to the software project that needs Nexus,
  but in general it is preferable to request a new, dedicated Openstack project.
  There is no need to create a container/bucket, only to request a proper amount of quota.
  An Openstack project can be used for multiple Nexus instances, which will share the quota.
1. Retrieve S3 keys from the [Openstack interface](https://openstack.cern.ch).
  In case of difficulties, please contact the [S3 Object Storage service](https://cern.ch/service-portal/function.do?name=S3ObjectStorage). These keys will be needed for creating the Nexus instance.
1. Create (if you don't have one already) an [e-group](https://cern.ch/e-groups) whose members will be granted admin
  privileges on the new instance. *Make sure that the creator of the instance is member of that group!* Note that
  only direct members of the group will taken into account (nested groups are not currently supported).
1. [Create a web site from the Web Services portal](https://cern.ch/webservices-help/Websitemanagement/ManagingWebsitesatCERN/Pages/WebsitecreationandmanagementatCERN.aspx)
  using type "PaaS Web Application" (Openshift).
1. Once you get the confirmation that the web site has been provisioned, navigate to your site details in
  the [Web Services portal](https://cern.ch/web) and follow the link to "Manage your site" to open the Openshift Console.
1. Select the `nexus-cern` item in the Catalog and fill in the values for S3 keys and admin e-groups prepared in the
  previous steps. Provisioning takes a few minutes.
1. The Nexus instance will be accessible at `https://<sitename>.web.cern.ch` and `https://cern.ch/<sitename>`

The configured e-group has full admin permissions on the provisioned Nexus instance. The default configuration is explained below.
See [Nexus3 help pages](https://help.sonatype.com/repomanager3/configuration) to configure Nexus and create artifact repositories.

## Service Level for Nexus data

Nexus artifact data is stored on [the CERN S3 service](https://cern.service-now.com/service-portal/article.do?n=KB0005539). Please
refer to the S3 service page for information about how data is stored. A dedicated S3 bucket `nexus-cern-<project name>` is created as part of
provisioning an instance.

Nexus also uses a database to store configuration. This database is stored on an
[Openshift standard persistent volume](https://cern.service-now.com/service-portal/article.do?n=KB0004361) and is backed up to S3
every evening between 21h and 22h.

Thus **an instance can be fully restored from the contents its S3 bucket**.

### Protection against accidental data deletion or modification

[Soft-deletion is configured](https://docs.aws.amazon.com/AmazonS3/latest/dev/Versioning.html) in Nexus S3 buckets so that deleted
or modified objects are kept for 30 days before being removed.
This allows Nexus instance owners to recover a previous state as long as the S3 bucket remains available.

To delete data permanently, one would need to explicitly remove soft-deleted content (or older object versions in general)
with specific tools and knowledge of the S3 account's access keys.

### Protection against (unlikely) catastrophic failure of the S3 service

The S3 data storage service does not currently (Feb 2019) have backups, so **Nexus data is ultimately subject to
[the CERN S3 service level](https://cern.service-now.com/service-portal/article.do?n=KB0005539)**. Each chunk of data is replicated
multiple times and scrubbed regularly so the risk of data loss is extremely low.

In case the S3 service level is considered insufficient for the data stored in a Nexus instance, for additional protection
instance owners may consider replicating the S3 bucket to an independent (and physically isolated) storage system.
Providing instructions to set up such a replication is out of the scope of this documentation but we invite Nexus instance owners
to [report their requirement for extra backup](https://cern.service-now.com/service-portal/service-element.do?name=soft-comp-rep)
to better understand the need for this extra protection.

Note that a second, independent S3 instance with a bucket replication service is already being considered and may provide a simple
solution in the future.

### Security updates

Security vulnerabilities are announced in https://support.sonatype.com/hc/en-us/articles/213465958-Sonatype-Nexus-Security-Advisories
(follow the article for notifications).

Security updates will be deployed centrally and restart all existing instances, with a short downtime (typically a few minutes).
Deployment of security updates will be announced on the [IT SSB](https://cern.ch/itssb).

## Initial configuration

### Authentication

A SSO proxy is in charge of performing user authentication, but takes no authorization decision. The following
authentication methods are implemented:

* for the Nexus web interface and API: SSO authentication with Shibboleth. No anonymous access is allowed. For API
  access in scripts, use [cern-get-sso-cookie](http://linux.web.cern.ch/linux/docs/cernssocookie.shtml)
* for repository access:
  * in general: LDAP authentication (i.e. user/password of a CERN account/service account)
  * certain repository types use token authentication instead
  * anonymous access if explicitly enabled in Nexus configuration (disabled by default)

NB: due to the design of the Nexus web interface, it is not possible to re-authenticate automatically with SSO when the Nexus session
expires. This means that once the user session expires, Nexus will complain that the user is not authenticated anymore and
**users need to refresh the page (typically pressing F5 or the reload button) in their web browser to re-authenticate**.
Nexus sessions are configured to expire after 4h by default but this can [be adjusted as necessary](https://groups.google.com/a/glists.sonatype.com/forum/#!topic/nexus-users/Spv3SVtO4Bs).

### Authorization

Authorization decisions take place in Nexus via [roles](https://help.sonatype.com/repomanager3/security/roles).
E-groups can be used by [Mapping External Groups to Nexus Roles](https://help.sonatype.com/repomanager3/security/roles#Roles-MappingExternalGroupstoNexusRoles).

E-groups are available under the preconfigured "CERN xLDAP" external group provider.

**Important note:** only direct e-group members are taken into account. Members of nested e-groups (groups included in a group)
are currently ignored.

### Blob storage

During instance provisioning, a S3 bucket `nexus-cern-<instance name>` is created with the user-provided S3 keys.
As a safeguard, instance provisioning will fail if such a bucket exists already and is not empty.

Then a S3 [Blob Store](https://help.sonatype.com/repomanager3/configuration/repository-management#RepositoryManagement-BlobStores)
is configured automatically to use that bucket.

As S3 scales very well, please make sure to **configure all repositories to use the default S3 blob store** and do not create any additional Blob Store.

The S3 bucket is configured by default with a [lifecycle policy](https://docs.aws.amazon.com/AmazonS3/latest/dev/object-lifecycle-mgmt.html)
that keeps deleted items for 30 days (soft-deletion).

### Nexus database

Nexus uses an OrientDB database to store configuration.
This database is backed up is stored in S3 every evening, and thanks to S3 versioning can be restored in a state up to 30 days prior.

## Design considerations

### Template

The Nexus application template is written as an [Ansible Playbook Bundle](https://docs.okd.io/latest/apb_devel/index.html).
Nexus does not use configuration files but a database. This means complex operations to configure Nexus during provisioning.
The template internally uses the [nexus3-oss Ansible role](https://github.com/ansible-ThoTeam/nexus3-oss) for this.

### Mitigation of duplicate provisioning

This Nexus APB is not currently re-entrant:
* the [`setup-s3-bucket` role](roles/setup-s3-bucket) includes sanity checks for the S3 bucket, e.g.
  so we don't re-use a non-empty bucket (to avoid inadvertently overwriting a previously existing instance).
* the `nexus3-oss` upstream role would need local persistent storage in the provisioner job to remember which Groovy scripts it already
  installed, and will fail if run again on an already configured instance without that local data.
* the `nexus3-oss` upstream role looks at `nexus_data_dir_contents` to determine whether an instance is already
  installed and take some decisions based on that. However the folder is not readily available for the provisioning APB.

This means that if a user tries to provision another
[ServiceInstance](https://docs.openshift.com/container-platform/3.11/architecture/service_catalog/index.html#service-catalog-concepts-terminology)
of Nexus in the same namespace, there will typically an error and the Kubernetes Service Catalog will
initiate [orphan mitigation](https://github.com/openservicebrokerapi/servicebroker/blob/b9bfda62c1ab195995892d61e641d0f4171a704e/spec.md#orphans).
This will result in deprovisioning of the previously existing instance.

We want to avoid this since the effort of recovering from this is non-trivial and this is really not the expected behavior
when mistakenly provisioning a duplicate instance.

While APBs would typically be re-entrant (just ensuring all resources are in the expected state),
it seems difficult to be fully re-entrant with Nexus due to the complexity of Nexus configuration, making it hard to be fail-safe.
For now, we're doing the following:
* use the present of data in the S3 bucket to determine that an instance exists already, and fail provisioning early in that case.
* make sure to set a label `nexus-cern-apb.cern.ch/instance-id` on each resource  with the specific ServiceInstance unique ID,
  and during deprovisioning only delete resources with the matching label. This will prevent the mitigation of the
  second provisioning that failed due to S3 checks from deleting the resources of the first instance.
* NB: this only works with an early failure in case of existing instance, since if a second provisioning starts taking place, the `nexus-cern-apb.cern.ch/instance-id`
  label on existing resources will just be overwritten with the new instance ID (and then removed by mitigation of failure in second provisioning).

## Cookbook

### Invoke API

For general API access, use [cern-get-sso-cookie](http://linux.web.cern.ch/linux/docs/cernssocookie.shtml) for authentication
with a CERN account.

It is also possible to use the Openshift CLI to bypass SSO authentication and use Nexus's local admin account.
E.g. this runs one of the scripts installed by the [nexus3-oss Ansible role](https://github.com/ansible-ThoTeam/nexus3-oss)
to delete a repo with name `central`:

```bash
# forward a local port to nexus, bypassing SSO (process runs in the background, remember to kill it when done)
oc port-forward dc/nexus 8081 &
adminpwd=$(oc get secret/nexus-admin-password -o go-template='{{index .data "nexusAdminPassword"}}' | base64 -d)
curl http://admin:${adminpwd}@localhost:8081/service/rest/v1/script/delete_repo/run -X POST -v -d '{"name": "central"}' -H 'Content-Type: text/plain'
```

### Slow Maven artefact downloads

It can take long for Maven to download all artefact from Nexus for each build.
When using GitLab CI, consider caching Maven artifacts: https://stackoverflow.com/questions/37785154/how-to-enable-maven-artifact-caching-for-gitlab-ci-runner

### Restore previous version of Nexus database

TODO
