**This is dummy role that cannot be used as-is.**

This folder stores custom files that need to be injected into the
upstream nexus3-oss role in order to deploy Nexus in a Kubernetes/Openshift
environment.

When the Docker image is built, these files are injected into the upstream nexus3-oss
role to create the final, customized nexus role.

TODO: there are probably cleaner ways to achieve this.